CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module adds a field formatter which allows a site builder to configure
the formatting of email fields. It's currently capable of outputting the e-mail
field as a mailto link with a custom title with token support.

REQUIREMENTS
------------

No specific requirements.

INSTALLATION
------------

Install as you would any other Drupal contrib module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------

Configure your email field type in:
admin/structure/types/manage/content_type/display

MAINTAINERS
-----------

Chris Jansen (legolasbo): https://www.drupal.org/u/legolasbo
